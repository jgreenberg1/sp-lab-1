#ifndef readfile_h
#define readfile_h

int open_file(const char s[64], FILE **fp);
int read_int(FILE *fp, int *x);
int read_float(FILE *fp, float *f);
int read_string(FILE *fp, char *s);
int close_file(FILE *fp);

#endif