#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "readfile.h"

/*Josh Greenberg - 3/31/21*/
/*This program reads from a text input file and stores the data there into employee records
which are then stored in the program where they can be printed, searched, and added to by a
user.*/

#define MAXNAME 65

struct employee /* employee struct holds id, first name, last name, and salary */
{
  int id_num;
  char first_name[MAXNAME];
  char last_name[MAXNAME];
  int salary;
};

/*This function is used to compare two employee records based on ID*/
int employee_comp(const void *emp1, const void *emp2)
{
  const struct employee *p1 = (struct employee *)emp1;
  const struct employee *p2 = (struct employee *)emp2;
  if (p1->id_num < p2->id_num)
  {
    return -1;
  } else if (p1->id_num > p2->id_num)
  {
    return 1;
  } else 
  {
    return 0;
  }
}

int employee_comp_by_sal(const void *emp1, const void *emp2)
{
  const struct employee *p1 = (struct employee *)emp1;
  const struct employee *p2 = (struct employee *)emp2;
  if (p1->salary < p2->salary)
  {
    return -1;
  } else if (p1->salary > p2->salary)
  {
    return 1;
  } else 
  {
    return 0;
  }
}

/*prototypes for functions below main*/
void first_choice(struct employee *db, int *count);
int second_choice(struct employee *db, int *count);
void third_choice(struct employee *db, int *count);
void fourth_choice(struct employee *db, int *count);
void sixth_choice(struct employee *db, int *count);
void seventh_choice(struct employee *db, int *count);
void eighth_choice(struct employee *db, int *count);
void ninth_choice(struct employee *db, int *count);
void dbsort(struct employee *db, int count);
void dbsort_bysal(struct employee*db, int count);

/*The main function of the program which takes in the input file from the command line,
puts the data therein into an array of employee structs, and then provides a menu of
options to the user.*/
int main(int argc, char const *argv[]) 
{
  
  if (argc != 2) /* check if we have valid number of inputs from command line */
  {
    printf("Cannot run without input file.\n");
  } else {
    FILE *inputFile = NULL; /*initialize file pointer*/

    if (open_file(argv[1], &inputFile) != 0) { /* check if we can open the input file */
      printf("This is not a valid input file.\n");
      return -1;
    } else {
      /*declare a bunch of relevant variables*/
      struct employee Database[1024];
      int emp_count = 0;
      int new_id;
      char new_first[MAXNAME];
      char new_last[MAXNAME];
      int new_sal;
      int ret;

      /*While we are not at the end of the file, contine to loop through it and extract
      data with our helper functions into our local vairables*/
      while (!feof(inputFile)) {
        ret = read_int(inputFile, &new_id);
        if (ret == -1) {
          break;
        }
        ret = read_string(inputFile, new_first);
        if (ret == -1) {
          break;
        }
        ret = read_string(inputFile, new_last);
        if (ret == -1) {
          break;
        }
        ret = read_int(inputFile, &new_sal);
        if (ret == -1) {
          break;
        }

        /*As long as we haven't broken out of the loop by now, we can add the data from our
        local variables into the employee database at the newest record*/
        Database[emp_count].id_num = new_id;
        strcpy(Database[emp_count].first_name, new_first);
        strcpy(Database[emp_count].last_name, new_last);
        Database[emp_count].salary = new_sal;

        /*Increment the index in our database array. If we are at the last record at this point,
        then this will still increment and will now hold the number of employees in the database.*/
        emp_count++;
      }
      close_file(inputFile); /*done reading the file, so close it.*/
      dbsort(Database, emp_count); /*sort our array using the sort function built for our array*/
      /* test code follows that was used to make sure we actually got the data we wanted
      i = 0;
      struct employee curr_emp;
      while (Database[i].id_num != 0) {
        curr_emp = Database[i];
        printf("%d %s %s %d\n", curr_emp.id_num, curr_emp.first_name, curr_emp.last_name, curr_emp.salary);
        i++;
      }*/

      /*Menu code*/
      int choice = 0;
      while (choice != 5) {
        printf("\nEmployee Database Menu: \n");
        printf("----------------------------------\n");
        printf("  (1) Print the Database\n");
        printf("  (2) Lookup by ID\n");
        printf("  (3) Lookup by Last Name\n");
        printf("  (4) Add an Employee\n");
        printf("  (5) Quit\n");
        printf("  (6) Remove an Employee\n");
        printf("  (7) Update an Employee's Information\n");
        printf("  (8) Print the M Employees with the Highest Salaries\n");
        printf("  (9) Find All Employees with Matching Last Name\n");
        printf("----------------------------------\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        if (choice > 9 || choice < 1)
        {
          printf("Hey, %d is not between 1 and 9! Try again\n", choice);
        } else if (choice == 1)
        {
          first_choice(Database, &emp_count);
        } else if (choice == 2) 
        {
          second_choice(Database, &emp_count);
        } else if (choice == 3)
        {
          third_choice(Database, &emp_count);
        } else if (choice == 4)
        {
          fourth_choice(Database, &emp_count);
        } else if  (choice == 5)
        {
          printf("Thank you, and goodbye.\n");
        } else if (choice == 6) 
        { 
          sixth_choice(Database, &emp_count);
        } else if (choice == 7)
        {
          seventh_choice(Database, &emp_count);
        } else if (choice == 8)
        {
          eighth_choice(Database, &emp_count);
        } else if (choice == 9)
        {
          ninth_choice(Database, &emp_count);
        } else {
          printf("That is not a valid option.\n");
        }
      }
      

    }
  }
  

  return 0;
}
/*Print Employee DB*/
void first_choice(struct employee *db, int *count)
{
  printf("\n");
  printf("NAME                                     SALARY      ID\n");
  printf("---------------------------------------------------------------\n");
  /*Loop through the employee records, printing out each one with appropriate formatting*/
  for (int i = 0; i < *count; ++i)
  {
    printf("%-20s %-20s %12d %12d\n", db[i].first_name, db[i].last_name, db[i].salary, db[i].id_num);
  }
  printf("---------------------------------------------------------------\n");
  printf("  Number of Employees (%d)\n", *count);
}

/*Search by ID*/
int second_choice(struct employee *db, int *count)
{
  int search_id; /*declare search variable*/
  do { /*while user still has not entered a valid search term, keep asking*/
    printf("\n");
    printf("Enter a 6 digit employee ID: ");
    scanf("%d", &search_id); 
  } while (search_id < 100000 || search_id > 999999);
  printf("\n");
  int i = -1; /*we increment this at the start of the next loop so it has to start at -1*/
  int found = 0; /*essentially a boolean value*/
  do 
  { /*linearly search through the array until we reach the end or find the search term*/
    i++;
    if (db[i].id_num == search_id)
    {
      found = 1;
    }
  } while (i < (*count - 1) && found == 0);
  if (found == 0) /*if we didn't find any records matching the search term, let the user know*/
  {
    printf("Unable to locate any employees with ID %d.\n", search_id);
    return -1;
  } else { /*otherwise, print the found record. i should store the index of that record*/
    printf("NAME                                     SALARY      ID\n");
    printf("---------------------------------------------------------------\n");
    printf("%-20s %-20s %12d %12d\n", db[i].first_name, db[i].last_name, db[i].salary, db[i].id_num);
    printf("---------------------------------------------------------------\n");
    return i;
  }
  
  
}
/*Search by Last Name*/
void third_choice(struct employee *db, int *count)
{
  char search_name[MAXNAME]; /*declare search term string*/
  printf("\n");
  printf("Enter Employee's last name (no extra spaces): "); /*ask user for search term*/
  scanf("%s", search_name);
  int i = -1; /*as in the second choice, this increments at the *start* of the next loop, so -1*/
  int found = 0; /*essentially a boolean*/
  do
  { /*loop until the strcmp function finds a match in our linear search, or we reach the end*/
    i++;
    if (strcmp(db[i].last_name, search_name) == 0)
    {
      found = 1;
    }
  } while (i < (*count -1) && found == 0);
  if (found == 0)
  { /*if we failed to find the search term, let the user know*/
    printf("No employees with last name %s\n", search_name);
  } else { /*otherwise print the found record.*/
    printf("NAME                                     SALARY      ID\n");
    printf("---------------------------------------------------------------\n");
    printf("%-20s %-20s %12d %12d\n", db[i].first_name, db[i].last_name, db[i].salary, db[i].id_num);
    printf("---------------------------------------------------------------\n");
  }
}

/*Add employee*/
void fourth_choice(struct employee *db, int *count)
{
  /*declare some local variables to store new data*/
  char new_first[MAXNAME];
  char new_last[MAXNAME];
  int new_sal;
  int sub_choice;
  printf("\n");
  do
  { /*ask the user for a valid first name, keep asking until valid*/
    printf("Enter first name of new employee: ");
    scanf("%s", new_first);
    if (strlen(new_first) > 64)
    {
      printf("That name is too long. Try again.\n");
    }
  } while (strlen(new_first) > 64);
  do
  { /*ask the user for a valid last name, keep asking until valid*/
    printf("Enter last name of new employee: ");
    scanf("%s", new_last);
    if (strlen(new_last) > 64)
    {
      printf("That name is too long. Try again.\n");
    }
  } while (strlen(new_last) > 64);
  do
  { /*ask the user for a valid employee salary, keep asking until within range*/
    printf("Enter salary of new employee (30000 to 150000): ");
    scanf("%d", &new_sal);
    if (new_sal > 150000 || new_sal < 30000)
    {
      printf("That salary is outside of the acceptable range. Try again.\n");
    }
  } while (new_sal > 150000 || new_sal < 30000);
  
  /*print the data the user just entered to check if the user actually wants to add it*/
  printf("Do you want to add the following employee to the database?\n");
  printf("\t%s %s, salary: %d\n", new_first, new_last, new_sal);
  printf("Enter 1 for yes, 0 for no: ");
  scanf("%d", &sub_choice);

  if (sub_choice == 1)
  { /*Add the local data to the database just after the last entry as determined by the count variable. */
    /*Assuming that record has the highest id, we increment it for the new id*/
    db[*count].id_num = db[*count - 1].id_num + 1;
    strcpy(db[*count].first_name, new_first);
    strcpy(db[*count].last_name, new_last);
    db[*count].salary = new_sal;
    *count = *count + 1; /*increment the number of employee records*/
    /*the record should already be sorted, but just to be safe...*/
    dbsort(db, *count);
  }

}

/*The fifth choice is simply quitting, therefore it is skipped here*/

/*Remove an employee*/
void sixth_choice(struct employee *db, int *count)
{
  /*Utilize second choice search function to return index of employee to delete*/
  int place = second_choice(db, count);
  if (place != -1) /*as long as we got a valid index, proceed*/
  {
    int affirm = 0; /*ask user if they are sure*/
    printf("Would you like to delete this employee record? (1 for yes, 0 for no): ");
    scanf("%d", &affirm);
    printf("\n");
    if (affirm == 1)
    { /*if they are sure, simply loop through the array starting at that index, changing each employee to the next*/
      do
      {
        db[place] = db[place + 1];
        place++;
      } while (place < *count); /*Until we reach the end of the array*/
      *count = *count - 1; /*decrement the count of employees (so last entry is "deleted"*/
      printf("Employee deleted.\n");
    }
  }
}

/*Update an Employee's Information*/
void seventh_choice(struct employee *db, int *count)
{
  int place = second_choice(db, count); /*User the search function to find employee*/
  if (place != -1) /*if we found the employee, run the function*/
  {
    /*ask the user if they want to update each field*/
    int affirm = 0; 
    printf("Would you like to update the Employee's First Name? (1 for yes, 0 for no): ");
    scanf("%d", &affirm);
    printf("\n");
    if (affirm == 1) /*if yes, ask for new first name, then update*/
    {
      char new_first[MAXNAME];
      do
      { /*ask the user for a valid first name, keep asking until valid*/
      printf("Enter new first name: ");
      scanf("%s", new_first);
      if (strlen(new_first) > 64)
      {
        printf("That name is too long. Try again.\n");
      }
      } while (strlen(new_first) > 64);
      strcpy(db[place].first_name, new_first); /*Update first name with new first name*/
    }
    /*Repeat for last name*/
    printf("Would you like to update the Employee's Last Name? (1 for yes, 0 for no): ");
    scanf("%d", &affirm);
    printf("\n");
    if (affirm == 1)
    {
      char new_last[MAXNAME];
      do
      { /*ask the user for a valid last name, keep asking until valid*/
      printf("Enter new last name: ");
      scanf("%s", new_last);
      if (strlen(new_last) > 64)
      {
        printf("That name is too long. Try again.\n");
      }
      } while (strlen(new_last) > 64);
      strcpy(db[place].last_name, new_last);
    }
    /*Repeat for salary*/
    printf("Would you like to update the Employee's Salary? (1 for yes, 0 for no): ");
    scanf("%d", &affirm);
    printf("\n");
    if (affirm == 1)
    {
      int new_sal;
      do
      { /*ask the user for a valid employee salary, keep asking until within range*/
        printf("Enter new salary for employee (30000 to 150000): ");
        scanf("%d", &new_sal);
        if (new_sal > 150000 || new_sal < 30000)
        {
          printf("That salary is outside of the acceptable range. Try again.\n");
        }
      } while (new_sal > 150000 || new_sal < 30000);
      db[place].salary = new_sal;
    }
  }
}

/*Print the M employees with the highest salary*/
void eighth_choice(struct employee *db, int *count)
{
  int top_emps = 0;
  do
  { /*Ask the user how many top earning employees we are looking for*/
    printf("The top how many employees do you want to show? ");
    scanf("%d", &top_emps);
    if (top_emps > *count || top_emps < 1)
    {
      printf("Number must be between 1 and the number of employees. Try again.\n");
    }
  } while (top_emps > *count || top_emps < 1);
  dbsort_bysal(db, *count); /*Sort the database by salary*/

  printf("\nNAME                                     SALARY      ID\n");
  printf("---------------------------------------------------------------\n");
  for (int i = (*count - 1); i > (*count - top_emps - 1); i--)
  { /*Loop through the database from the end M times, printing out the employee*/
    printf("%-20s %-20s %12d %12d\n", db[i].first_name, db[i].last_name, db[i].salary, db[i].id_num);
  }
  printf("---------------------------------------------------------------\n");
  dbsort(db, *count); /*Sort the database by ID's again before we go back*/
}

/*Find all employees with matching last name*/
void ninth_choice(struct employee *db, int  *count)
{
  char search_name[MAXNAME]; /*declare search term string*/
  char comp_name[MAXNAME];
  int found = 0;
  printf("\n");
  printf("Enter last name to search for (no extra spaces): "); /*ask user for search term*/
  scanf("%s", search_name);
  for (int i = 0; search_name[i]; ++i)
  { /*convert search name to lower case*/
    search_name[i] = tolower(search_name[i]);
  }
  printf("\nNAME                                     SALARY      ID\n");
  printf("---------------------------------------------------------------\n");
  for (int i = 0; i < *count; ++i)
  {
    strcpy(comp_name, db[i].last_name); /*get a copy of the last name...*/
    for (int i = 0; comp_name[i]; ++i)
    { /*...and convert that copy to lowercase*/
      comp_name[i] = tolower(comp_name[i]);
    }
    if (strcmp(comp_name, search_name) == 0) /*then if we have a match, print the employee*/
    {
      printf("%-20s %-20s %12d %12d\n", db[i].first_name, db[i].last_name, db[i].salary, db[i].id_num);
      found = 1; /*we say we found at least one employee*/
    }
  } 
  if (found == 0)
  { /*if we didn't find any employees, tell the user as much*/
    printf("No employee with that last name was found.\n");
  }
  printf("---------------------------------------------------------------\n");
}

/*Helper function to sort DB by ID*/
void dbsort(struct employee *db, int count)
{
  qsort(db, count, sizeof(struct employee), employee_comp);
}
/*And Helper function to sort DB by salary*/
void dbsort_bysal(struct employee *db, int count)
{
  qsort(db, count, sizeof(struct employee), employee_comp_by_sal);
}


