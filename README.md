To compile the readfile library:

gcc -c -Wall -Werror -fpic readfile.c

ar rcs libreadfile.a readfile.o

To compile the main program:

gcc -c -Wall -Werror main.c -o main.o

gcc -Wall -o main main.o -L. -lreadfile

To run the file:

./main employees.txt