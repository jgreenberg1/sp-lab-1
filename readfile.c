#include <stdio.h>
#include <string.h>

/*Josh Greenberg - 3/31/2021*/
/*This readfile library adds some simplicity to reading a text file and extracting ints,
floats, and strings from it into local variables.*/

/*This function accepts a string and a pointer to a file pointer, attempts to open the file
at that file name with that file pointer, and then returns 0 on success, or -1 on failure.*/
int open_file(const char s[64], FILE **fp){

  if ((*fp = fopen(s, "r")))
   {
     return 0;
   } else {
    return -1;
   }

}

/*This function accepts a file pointer and a pointer to a int variable. If the file pointer
is not at the end of the file, it uses fscanf to find the next int in the file and put it
in the supplied variable, then returns 0. Otherwise, it returns -1.*/
int read_int(FILE *fp, int *x) {
  if (!feof(fp))
  {
    fscanf(fp, "%d", x);
    return 0;
  } else {
    return -1;
  }
}

/*This function accepts a file pointer and a pointer to a float variable. If the file pointer
is not at the end of the file, it uses fscanf to find the next float in the file and put it
in the supplied variable, then returns 0. Otherwise, it returns -1.*/
int read_float(FILE *fp, float *f) {
  if (!feof(fp))
  {
    fscanf(fp, "%f", f);
    return 0;
  } else {
    return -1;
  }
}

/*This function accepts a file pointer and a pointer to a string variable. If the file pointer
is not at the end of the file, it uses fscanf to find the next string in the file and put it
in the supplied variable, then returns 0. Otherwise, it returns -1.*/
int read_string(FILE *fp, char *s) {
  
  /*fscanf(fp, "%s[^ ] ", s);*/
  if (!feof(fp))
  {
    fscanf(fp, "%s", s);
    return 0;
  } else {
    return -1;
  }
}

/*This functions closes the file at the file pointer supplied.*/
int close_file(FILE *fp) {
  return fclose(fp);
}
